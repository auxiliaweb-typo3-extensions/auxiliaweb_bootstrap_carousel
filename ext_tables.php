<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}


\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Auxiliaweb.AuxiliawebBootstrapCarousel',
    'Carousel',
    'Bootstrap Carousel'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('auxiliaweb_bootstrap_carousel', 'Configuration/TypoScript', 'Bootstrap Carousel');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_auxiliawebbootstrapcarousel_domain_model_carousel', 'EXT:auxiliaweb_bootstrap_carousel/Resources/Private/Language/locallang_csh_tx_auxiliawebbootstrapcarousel_domain_model_carousel.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_auxiliawebbootstrapcarousel_domain_model_carousel');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_auxiliawebbootstrapcarousel_domain_model_carouselitem', 'EXT:auxiliaweb_bootstrap_carousel/Resources/Private/Language/locallang_csh_tx_auxiliawebbootstrapcarousel_domain_model_carouselitem.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_auxiliawebbootstrapcarousel_domain_model_carouselitem');

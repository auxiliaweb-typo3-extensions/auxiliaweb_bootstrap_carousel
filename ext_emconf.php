<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Bootstrap Carousel',
    'description' => 'Creates a bootstrap carousel from tt_content images.',
    'category' => 'plugin',
    'author' => 'Philipp Schumann',
    'author_email' => 'ph.schumann@gmx.de',
    'state' => 'beta',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '10.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '10.0.0-10.99.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];

<?php
namespace Auxiliaweb\AuxiliawebBootstrapCarousel\Controller;

/***
 *
 * This file is part of the "Bootstrap Carousel" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Philipp Schumann <ph.schumann@gmx.de>
 *
 ***/

/**
 * CarouselController
 */
class CarouselController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * carouselRepository
     * 
     * @var \Auxiliaweb\AuxiliawebBootstrapCarousel\Domain\Repository\CarouselRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $carouselRepository;

    /**
     * @var \Auxiliaweb\AuxiliawebBootstrapCarousel\Utility\CarouselHelper
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $carouselHelper;

    /**
     * action show
     * 
     * @return void
     */
    public function showAction()
    {
        $carousel = $this->carouselRepository->findByUid($this->carouselHelper->getCarousel());
        $this->view->assign('carousel', $carousel);
        $this->view->assign('hideIndicators', $this->carouselHelper->getHideIndicators());
        $this->view->assign('hidePrevNextControls', $this->carouselHelper->getHidePrevNextControls());
        $this->view->assign('hideCaptions', $this->carouselHelper->getHideCaptions());
        $this->view->assign('bootstrapVersion', $this->carouselHelper->getBootstrapVersion());
        $this->view->assign('intervall', $this->carouselHelper->getIntervall());
    }
}

<?php
namespace Auxiliaweb\AuxiliawebBootstrapCarousel\Utility;

use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

/***
 *
 * This file is part of the "Bootstrap Carousel" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Philipp Schumann <ph.schumann@gmx.de>
 *
 ***/

/**
 * WatchwordSettings
 */
class CarouselHelper {

    /**
     * Settings
     *
     * @var array
     */
    protected $settings = [];

    /**
     * Returns the carousel
     *
     * @return int
     */
    public function getCarousel() {
        return $this->settings['settings']['carousel'];
    }

    /**
     * Returns the info about hiding indicators
     *
     * @return bool
     */
    public function getHideIndicators() {
        return $this->settings['settings']['hideIndicators'];
    }

    /**
     * Returns the info about hiding indicators
     *
     * @return bool
     */
    public function getHidePrevNextControls() {
        return $this->settings['settings']['hidePrevNextControls'];
    }

    /**
     * Returns the info about hiding captions
     *
     * @return bool
     */
    public function getHideCaptions() {
        return $this->settings['settings']['hideCaptions'];
    }

    /**
     * Returns the info about the selected Bootstap version
     *
     * @return string
     */
    public function getBootstrapVersion() {
        return $this->settings['settings']['bootstrapVersion'] ?: 'none';
    }

    /**
     * Returns the info about the selected Bootstap version
     *
     * @return int
     */
    public function getIntervall() {
        return $this->settings['settings']['intervall'] ?: 3000;
    }

    /**
     * Inject settings via ConfigurationManager.
     *
     * @param ConfigurationManagerInterface $configurationManager
     */
    public function injectConfigurationManager(ConfigurationManagerInterface $configurationManager) {
        $this->settings = $configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK
        );
    }
}

<?php

namespace Auxiliaweb\AuxiliawebBootstrapCarousel\Domain\Model;

/***
 *
 * This file is part of the "Bootstrap Carousel" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Philipp Schumann <ph.schumann@gmx.de>
 *
 ***/

/**
 * CarouselItem
 */
class CarouselItem extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * text
     *
     * @var string
     */
    protected $text = '';

    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $image;

    /**
     * additionalCssClass
     *
     * @var string
     */
    protected $additionalCssClass = '';

    /**
     * @return string
     */
    public function getTitle(): string {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title) {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getText(): string {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text) {
        $this->text = $text;
    }

    /**
     * Get the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Set image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function setFalMedia(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $image) {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getAdditionalCssClass(): string {
        return $this->additionalCssClass;
    }

    /**
     * @param string $additionalCssClass
     */
    public function setAdditionalCssClass(string $additionalCssClass) {
        $this->additionalCssClass = $additionalCssClass;
    }

}

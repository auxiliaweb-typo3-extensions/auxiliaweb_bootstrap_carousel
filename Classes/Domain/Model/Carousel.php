<?php

namespace Auxiliaweb\AuxiliawebBootstrapCarousel\Domain\Model;

/***
 *
 * This file is part of the "Bootstrap Carousel" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Philipp Schumann <ph.schumann@gmx.de>
 *
 ***/

/**
 * Carousel
 */
class Carousel extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {
    /**
     * carouselItem
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Auxiliaweb\AuxiliawebBootstrapCarousel\Domain\Model\CarouselItem>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade
     */
    protected $carouselItem = null;

    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * additionalCssClass
     *
     * @var string
     */
    protected $additionalCssClass = '';

    /**
     * __construct
     */
    public function __construct() {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects() {
        $this->carouselItem = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the carouselItem
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Auxiliaweb\AuxiliawebBootstrapCarousel\Domain\Model\CarouselItem> $carouselItem
     */
    public function getCarouselItem() {
        return $this->carouselItem;
    }

    /**
     * Sets the carouselItem
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Auxiliaweb\AuxiliawebBootstrapCarousel\Domain\Model\CarouselItem> $carouselItem
     * @return void
     */
    public function setCarouselItem(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $carouselItem) {
        $this->carouselItem = $carouselItem;
    }

    /**
     * @return string
     */
    public function getTitle(): string {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title) {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getAdditionalCssClass(): string {
        return $this->additionalCssClass;
    }

    /**
     * @param string $additionalCssClass
     */
    public function setAdditionalCssClass(string $additionalCssClass) {
        $this->additionalCssClass = $additionalCssClass;
    }
}

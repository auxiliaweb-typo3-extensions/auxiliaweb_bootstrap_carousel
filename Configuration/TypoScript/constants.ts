
plugin.tx_auxiliawebbootstrapcarousel_carousel {
    view {
        # cat=plugin.tx_auxiliawebbootstrapcarousel_carousel/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:auxiliaweb_bootstrap_carousel/Resources/Private/Templates/
        # cat=plugin.tx_auxiliawebbootstrapcarousel_carousel/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:auxiliaweb_bootstrap_carousel/Resources/Private/Partials/
        # cat=plugin.tx_auxiliawebbootstrapcarousel_carousel/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:auxiliaweb_bootstrap_carousel/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_auxiliawebbootstrapcarousel_carousel/a; type=string; label=Default storage PID
        storagePid =
    }
    settings {
        # cat = plugin.tx_auxiliawebbootstrapcarousel_carousel/a; type=string; label=Carousel ID
        carousel =
        # cat = plugin.tx_auxiliawebbootstrapcarousel_carousel/a; type=boolean; label=Hide Indicators
        hideIndicators =
        # cat = plugin.tx_auxiliawebbootstrapcarousel_carousel/a; type=boolean; label=Hide Prev- and Next-Controls
        hidePrevNextControls =
        # cat = plugin.tx_auxiliawebbootstrapcarousel_carousel/a; type=boolean; label=Hide Captions
        hideCaptions =
        # cat = plugin.tx_auxiliawebbootstrapcarousel_carousel/a; type=string; label=Bootstrap Version
        bootstrapVersion =
        # cat = plugin.tx_auxiliawebbootstrapcarousel_carousel/a; type=string; label=Intervall
        intervall =
    }
}

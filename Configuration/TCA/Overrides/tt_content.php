<?php
defined('TYPO3_MODE') or die();

/***************
 * Plugin
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'AuxiliawebBootstrapCarousel',
    'Carousel',
    'Bootstrap Carousel'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['auxiliawebbootstrapcarousel_carousel'] = 'recursive,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['auxiliawebbootstrapcarousel_carousel'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('auxiliawebbootstrapcarousel_carousel',
    'FILE:EXT:auxiliaweb_bootstrap_carousel/Configuration/FlexForms/flexform.xml');

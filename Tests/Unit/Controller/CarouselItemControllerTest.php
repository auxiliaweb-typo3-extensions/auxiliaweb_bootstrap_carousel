<?php
namespace Auxiliaweb\AuxiliawebBootstrapCarousel\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Philipp Schumann <ph.schumann@gmx.de>
 */
class CarouselItemControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Auxiliaweb\AuxiliawebBootstrapCarousel\Controller\CarouselItemController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Auxiliaweb\AuxiliawebBootstrapCarousel\Controller\CarouselItemController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllCarouselItemsFromRepositoryAndAssignsThemToView()
    {

        $allCarouselItems = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $carouselItemRepository = $this->getMockBuilder(\::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $carouselItemRepository->expects(self::once())->method('findAll')->will(self::returnValue($allCarouselItems));
        $this->inject($this->subject, 'carouselItemRepository', $carouselItemRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('carouselItems', $allCarouselItems);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }
}

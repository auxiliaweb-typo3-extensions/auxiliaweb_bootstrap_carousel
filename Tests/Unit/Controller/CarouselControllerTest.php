<?php
namespace Auxiliaweb\AuxiliawebBootstrapCarousel\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Philipp Schumann <ph.schumann@gmx.de>
 */
class CarouselControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Auxiliaweb\AuxiliawebBootstrapCarousel\Controller\CarouselController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Auxiliaweb\AuxiliawebBootstrapCarousel\Controller\CarouselController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllCarouselsFromRepositoryAndAssignsThemToView()
    {

        $allCarousels = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $carouselRepository = $this->getMockBuilder(\Auxiliaweb\AuxiliawebBootstrapCarousel\Domain\Repository\CarouselRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $carouselRepository->expects(self::once())->method('findAll')->will(self::returnValue($allCarousels));
        $this->inject($this->subject, 'carouselRepository', $carouselRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('carousels', $allCarousels);
        $this->inject($this->subject, 'view', $view);

        $this->subject->showAction();
    }
}

<?php
namespace Auxiliaweb\AuxiliawebBootstrapCarousel\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Philipp Schumann <ph.schumann@gmx.de>
 */
class CarouselItemTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Auxiliaweb\AuxiliawebBootstrapCarousel\Domain\Model\CarouselItem
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Auxiliaweb\AuxiliawebBootstrapCarousel\Domain\Model\CarouselItem();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function dummyTestToNotLeaveThisFileEmpty()
    {
        self::markTestIncomplete();
    }
}

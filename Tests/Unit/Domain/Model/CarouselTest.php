<?php
namespace Auxiliaweb\AuxiliawebBootstrapCarousel\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Philipp Schumann <ph.schumann@gmx.de>
 */
class CarouselTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Auxiliaweb\AuxiliawebBootstrapCarousel\Domain\Model\Carousel
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Auxiliaweb\AuxiliawebBootstrapCarousel\Domain\Model\Carousel();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getCarouselItemReturnsInitialValueForCarouselItem()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getCarouselItem()
        );
    }

    /**
     * @test
     */
    public function setCarouselItemForObjectStorageContainingCarouselItemSetsCarouselItem()
    {
        $carouselItem = new \Auxiliaweb\AuxiliawebBootstrapCarousel\Domain\Model\CarouselItem();
        $objectStorageHoldingExactlyOneCarouselItem = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneCarouselItem->attach($carouselItem);
        $this->subject->setCarouselItem($objectStorageHoldingExactlyOneCarouselItem);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneCarouselItem,
            'carouselItem',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addCarouselItemToObjectStorageHoldingCarouselItem()
    {
        $carouselItem = new \Auxiliaweb\AuxiliawebBootstrapCarousel\Domain\Model\CarouselItem();
        $carouselItemObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $carouselItemObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($carouselItem));
        $this->inject($this->subject, 'carouselItem', $carouselItemObjectStorageMock);

        $this->subject->addCarouselItem($carouselItem);
    }

    /**
     * @test
     */
    public function removeCarouselItemFromObjectStorageHoldingCarouselItem()
    {
        $carouselItem = new \Auxiliaweb\AuxiliawebBootstrapCarousel\Domain\Model\CarouselItem();
        $carouselItemObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $carouselItemObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($carouselItem));
        $this->inject($this->subject, 'carouselItem', $carouselItemObjectStorageMock);

        $this->subject->removeCarouselItem($carouselItem);
    }
}

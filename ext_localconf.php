<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Auxiliaweb.AuxiliawebBootstrapCarousel',
    'Carousel',
    [
        'Carousel' => 'show'
    ],
    // non-cacheable actions
    [
        'Carousel' => '',
        'CarouselItem' => ''
    ]
);

// wizards
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    carousel {
                        iconIdentifier = auxiliaweb_bootstrap_carousel-plugin-carousel
                        title = LLL:EXT:auxiliaweb_bootstrap_carousel/Resources/Private/Language/locallang_db.xlf:tx_auxiliaweb_bootstrap_carousel_carousel.name
                        description = LLL:EXT:auxiliaweb_bootstrap_carousel/Resources/Private/Language/locallang_db.xlf:tx_auxiliaweb_bootstrap_carousel_carousel.description
                        tt_content_defValues {
                            CType = list
                            list_type = auxiliawebbootstrapcarousel_carousel
                        }
                    }
                }
                show = *
            }
       }'
);

$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

$iconRegistry->registerIcon(
    'auxiliaweb_bootstrap_carousel-plugin-carousel',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:auxiliaweb_bootstrap_carousel/Resources/Public/Icons/user_plugin_carousel.svg']
);
		
